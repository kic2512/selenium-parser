# coding=utf-8

import urlparse
from time import sleep

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import ActionChains
from selenium.webdriver.common.alert import Alert
from os import getcwd

from tests.constants import TEST_USER_EMAIL, TEST_USER_PASSWD

__author__ = 'kic'


def down_keys(driver, text):
    ActionChains(driver).send_keys(text).perform()


class Page(object):
    BASE_URL = 'https://ru.fifa.com/worldcup/organisation/ticketing/ticket-availability.html'
    PATH = ''
    USERNAME = '//a[@class="username"]'

    def __init__(self, driver):
        self.driver = driver

    def open(self):
        url = urlparse.urljoin(self.BASE_URL, self.PATH)
        self.driver.get(url)

    def go_main_page(self):
        self.driver.get(self.BASE_URL)

    def get_username_from_settings(self):
        return WebDriverWait(self.driver, 10, 0.1).until(
            lambda d: d.find_element_by_xpath(self.USERNAME).text
        )

    def get_user_href_from_settings(self):
        return WebDriverWait(self.driver, 10, 0.1).until(
            lambda d: d.find_element_by_xpath(self.USERNAME).get_attribute('href')
        )


class WdayHeader(Page):
    LOGIN_BUTTON_X_PATH = '//button[@rel="popup-reg"]'

    EMAIL_INPUT_X_PATH = '//input[@name="login"]'
    PASSWD_INPUT_X_PATH = '//input[@name="password"]'

    LOGIN_SEND_BUTTON_X_PATH = '//button[text()="Войти"]'

    USER_PROFILE_LINK = '//a[@class="header__userLink"]'

    def authorize(self, login, passwd):

        self.driver.find_element_by_xpath(self.LOGIN_BUTTON_X_PATH).click()

        sleep(.2)

        self.driver.find_element_by_xpath(self.EMAIL_INPUT_X_PATH).send_keys(TEST_USER_EMAIL)
        self.driver.find_element_by_xpath(self.PASSWD_INPUT_X_PATH).send_keys(TEST_USER_PASSWD)

        self.driver.find_element_by_xpath(self.LOGIN_SEND_BUTTON_X_PATH).click()

    def get_profile_link(self):
        return self.driver.find_element_by_xpath(self.USER_PROFILE_LINK).get_attribute('href')
