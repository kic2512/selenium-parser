# coding=utf-8
import json
import smtplib
from collections import OrderedDict
from email.header import Header
from time import sleep
from unittest import TestCase
from selenium import webdriver
from page_object import Page
import os
import sys
from bs4 import BeautifulSoup
from selenium.webdriver.chrome.options import Options

os.environ["DISPLAY"] = ":99"

__author__ = 'kic'


class HSMTestBase(TestCase):
    def setUp(self):
        if sys.platform == 'darwin':
            self.driver = webdriver.Chrome()
        else:
            chrome_driver_binary = "/usr/bin/chromedriver"
            chrome_options = Options()
            chrome_options.add_argument("--headless")
            chrome_options.add_argument("--window-size=1920x1080")
            chrome_options.add_argument("start-maximized")  # // open Browser in maximized mode
            chrome_options.add_argument("disable-infobars")  # // disabling infobars
            chrome_options.add_argument("--disable-extensions")  # // disabling extensions
            chrome_options.add_argument("--disable-gpu")  # // applicable to windows os only
            chrome_options.add_argument("--disable-dev-shm-usage")  # // overcome limited resource problems
            chrome_options.add_argument("--no-sandbox")  # // Bypass OS security model
            self.driver = webdriver.Chrome(chrome_driver_binary, chrome_options=chrome_options)
        self.driver.implicitly_wait(3)
        self.driver.set_window_size(1920, 1080)

    def tearDown(self):
        self.driver.quit()


class TestAuthorize(HSMTestBase):
    def setUp(self):
        super(TestAuthorize, self).setUp()
        self.page = Page(self.driver)

    def test_login(self):
        """
        {
            'slug': {
                title: "Матч 63 - Матч за 3−е место - Санкт-Петербург"
                'CAT 1': True,
                'CAT 2': False,
                'CAT 3': True,
                'CAT 4': False,
                'OV': False,
            }
        }

        """
        self.page.open()
        pause = 1
        self.driver.execute_script("window.scrollTo(0, 800)")
        sleep(pause)
        el = self.driver.find_element_by_xpath(
            '//iframe[@data-src="https://tickets.fifa.com/Services/ADService.html?lang=ru"]')
        el.location_once_scrolled_into_view
        self.driver.switch_to_frame(self.driver.find_element_by_xpath(
            '//iframe[@data-src="https://tickets.fifa.com/Services/ADService.html?lang=ru"]'))

        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")

        data = {}
        previous_data = {}

        try:
            f = open('/tmp/tickets.dump', 'r')
            dump = f.read()
            f.close()
        except IOError:
            pass
        else:
            if dump:
                previous_data = json.loads(dump)

        letter_title = Header(u'ВНИМАНИЕ! На официальном сайте FIFA в продаже появились билеты на ЧМ2018!', 'utf-8')

        tickets_part_1 = self.driver.find_elements_by_class_name('rowBoxEven')
        tickets_part_2 = self.driver.find_elements_by_class_name('rowBoxOdd')

        tickets = tickets_part_1 + tickets_part_2

        for ticket in tickets:

            soup = BeautifulSoup(ticket.get_attribute('innerHTML'))
            available_categories = set()

            for child in soup.children:
                if child:
                    div = child.find_next('div', attrs={'class': 'header'})
                    if div:
                        title = div.text

                        category_box_container = child.find_next('div', attrs={'class': 'col-xs-12'})
                        key = None

                        for category_box in category_box_container.children:
                            item = category_box.find_next('div', attrs={'class': 'categoryBox'})

                            if item:
                                if 'zeroAvailability' not in item.attrs['class']:
                                    key = title[:7]
                                    category = item.text

                                    if key in data:
                                        if category in data[key]:
                                            if not data[key][category]:
                                                available_categories.add(category)
                                        else:
                                            data[key][category] = True
                                            available_categories.add(category)
                                    else:
                                        data[key] = {
                                            'title': title,
                                            'CAT 1': False,
                                            'CAT 2': False,
                                            'CAT 3': False,
                                            'CAT 4': False,
                                            'OV': False,
                                        }

                                        data[key][category] = True
                                        available_categories.add(category)

                                        # print('!! ----------------------')
                                        # print(title)
                                        # print(item.text)
                                        # print(item.attrs['class'])
                                        # print('---------------------- !!')

                        if available_categories and key:
                            cats = [x for x in available_categories]
                            cats.sort()

        letter_message = u''
        for data_key in data:
            free_categories = set()
            has_diff = False
            if data_key not in previous_data:
                for field in data[data_key]:
                    if isinstance(field, (int,)):
                        free_categories.add(field)
                        has_diff = True
            else:
                for field in data[data_key]:
                    if not previous_data[data_key][field] and data[data_key][field]:
                        free_categories.add(field)
                        has_diff = True

            if has_diff:
                letter_message += u'\n{0} {1}'.format(data[data_key]['title'], u' '.join(free_categories))

        print(letter_message)

        server_ssl = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        server_ssl.ehlo()  # optional, called by login()
        server_ssl.login(u'kravcov2512@gmail.com', u'@k25121992k@')
        # ssl server doesn't support or need tls, so don't call server_ssl.starttls()

        text = u"""From: {0}\nTo: {1}\nSubject: {2}\n\n{3}""".format(
            u'kravcov2512@gmail.com',
            u'idakraftsoft@gmail.com',
            letter_title,
            letter_message
        )

        server_ssl.sendmail(u'kravcov2512@gmail.com', u'idakraftsoft@gmail.com', text.encode('utf-8'))
        # server_ssl.quit()
        server_ssl.close()

        with open('/tmp/tickets.dump', 'w+') as f:
            f.write(json.dumps(data))

        self.assertTrue(tickets)
