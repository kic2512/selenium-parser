Wday autotests
=================

### Быстрый старт

1. Скачать chromedriver под операционную систему,    
на которой будут запускаться тесты 
https://sites.google.com/a/chromium.org/chromedriver/downloads   
2. Добавить путь до chromedriver в переменную окружения PATH,  
или скопировать chromedriver в одну из директорий указанных в PATH,  
пример для MacOS:    
cp ~/Downloads/chromedriver  /user/local/bin  
3. 
